from beancount.core import data as bcdata
from beancount.core.amount import Amount
from beancount.core.number import D as Decimal
from beancount.parser import printer
from beancount.loader import load_file
import datetime as dt
import csv, re, pickle
import sys, os
from fuzzywuzzy import fuzz as fz

class SECU_Importer(object):
    """An importer for NC SECU."""

    _header = ['Process Dates', 
               'Check Number', 
               'Description', 
               'Credit Amount', 
               'Debit Amount']
    _account_root = "Assets:Current-Assets:Joint-Checking"
    _pos_regexp = (r'Point of Sale\s+(?P<type>(?:Debit)|(?:Credit))\s+' +
                   r'(?P<lCode>L\d+)\s+' +
                   r'(?:TIME\s+(?P<time>\d+:\d+)\s+(?:(?:AM)|(?:PM))\s+)?' +
                   r'(?:DATE\s+(?P<date>\d+-\d+))\s+' +
                   r'(?P<description>.*)')
    _ach_regexp = (r'ACH\s+(?P<type>(?:Deposit)|(?:Debit))\s+'
                   r'(?:P<description>.*)')
    _date_regexp = (r'(?P<month>\d+)(?P<sep>[-/])' +
                   r'(?P<day>\d+)(?P=sep)' +
                   r'(?P<year>\d+)')

    _config_dir = os.path.expanduser("~/.config/beancount_importers")
    _config_fname = "secu_importer.pkl"
    _picklePath = os.path.join(_config_dir, _config_fname)


    # def name(self):
        # return"NC_SECU"

    # def identify(self, fCache):
        # header_string = ','.join(header)
        # check = fCache.head(sys.getsizeof(header_string))
        # return check  == header_string
    def __init__(self, dataFileName, journalFileName=None):
        self.fname = dataFileName

        self._journalEntries = None
        if not journalFileName is None:
            entries, _, _ = load_file(journalFileName)
            self._journalEntries = []
            for entry in entries:
                if isinstance(entry, bcdata.Transaction):
                    self._journalEntries.append(entry)

        if os.path.isfile(self._picklePath):
            with open(self._picklePath, 'rb') as infile:
                self._cache = pickle.load(infile)
        else:
            self._cache = {}
            if not os.path.exists(self._config_dir):
                os.makedirs(self._config_dir)

    def extract(self):
        with open(self.fname ,'r') as infile:
            data = csv.DictReader(infile)
            for row in data:
                rawDate        = row['Process Dates']
                checkNumber    = row['Check Number']
                rawDescription = row['Description']
                rawAmount      = '-'+row['Credit Amount'] if row['Debit Amount'] == '' else row['Debit Amount']

                print('-'*80)
                key = self._processDescription(rawDescription, rawAmount)
                found = self._checkExistingTransactions(key, rawAmount)
                if not found:
                    self._promptPostings(key, rawAmount)
                else:
                    self._promptCachedPostings(key, rawAmount)

                # Process the date
                dateMatch = re.match(SECU_Importer._date_regexp, rawDate)
                if dateMatch is None:
                    print('Could not parse date, skipping. date = {}'.format(rawDate))
                    continue;

                # create transaction object
                date = dt.date(int(dateMatch.group('year')),
                               int(dateMatch.group('month')),
                               int(dateMatch.group('day')))
                txn = bcdata.Transaction(None, 
                                         date, 
                                         "*", 
                                         None, 
                                         self._cache[key]['description'], 
                                         None, 
                                         None, 
                                         self._cache[key]['postings'])
                self._writeEntry(txn)

    def _processDescription(self, rawDescription, rawAmount):
        # First, see if we match any of our regexps
        posMatch = re.match(SECU_Importer._pos_regexp, rawDescription)
        achMatch = re.match(SECU_Importer._ach_regexp, rawDescription)
        if not posMatch is None:
            rawDescription = posMatch.group('description')
        elif not achMatch is None:
            rawDescription = achMatch.group('description')

        key = self._checkCache(rawDescription)
        if key is None:
            key = self._promptDescription(rawDescription)
        return key

    def _checkCache(self, rawDescription):
        # Next, see if our description matches any of our cached keys
        for key in self._cache.keys():
            check = re.sub('\s+', ' ', key.lower())
            test = re.sub('\s+', ' ', rawDescription.lower())
            result = fz.partial_token_sort_ratio(check, test)
            if result >= 85:
                print('"{}" matches\n'
                      '"{}"\n'
                      '   Match % = {}.'.format(key, rawDescription, result)
                        )
                if result < 100:
                    keep = input('Keep?  (blank or y for yes)\n-> ')
                else:
                    print("  keeping due to 100%\n")
                    keep = 'y'

                if len(keep) == 0 or keep[0].lower() == 'y':
                    return key
        return None

    def _promptDescription(self, rawDescription):
        description = input('No match found for "{}"\n  New description: (blank for no change)\n-> '.format(rawDescription))
        if len(description) == 0:
            description = rawDescription
        print("  Storing in cache")
        self._cache[rawDescription] = {'description':description}
        with open(self._picklePath, 'wb') as outfile:
            pickle.dump(self._cache, outfile)
        return rawDescription

    def _promptPostings(self, key, rawAmount):
        expense = input('  Expense account for this description? ')
        pay = input('  Pay account for this description? ')

        print("Saving: \n"
               "    description = {}\n"
               "    expense = {}\n"
               "    pay = {}\n"
               "    key = {}\n"
               "Edit {} to change.\n\n".format(self._cache[key]['description'], 
                                               expense, 
                                               pay, 
                                               key, 
                                               self._picklePath))
        amt1 = Amount(Decimal(rawAmount), "USD")
        pst1 = bcdata.Posting(expense, amt1, None, None, None, None)
        pst2 = bcdata.Posting(pay, None, None, None, None, None)
        self._cache[key]['postings'] = [pst1, pst2] 
        with open(self._picklePath, 'wb') as outfile:
            pickle.dump(self._cache, outfile)

    def _checkExistingTransactions(self, key, rawAmount):
        description = self._cache[key]['description']
        if self._journalEntries is None:
            return False

        for entry in reversed(self._journalEntries):
            result = fz.partial_token_sort_ratio(description, entry.narration)
            if result >= 95:
                print("Found matching transaction:\n")
                printer.print_entry(entry, file=sys.stdout)
                postings = entry.postings.copy()
                ans = input("Keep? (blank or y for yes) ")
                if len(ans) == 0 or ans.lower()[0] == 'y':
                    self._cache[key]['postings'] = postings
                    self._updateFirstPosting(key, rawAmount)
                    # Make last posting have zero amount
                    self._clearLastPosting(key)
                    with open(self._picklePath, 'wb') as outfile:
                        pickle.dump(self._cache, outfile)
                    return True
                else:
                    ans = input("  Keep searching? (blank or n for no) ")
                    if len(ans) == 0 or ans.lower()[0] == 'n':
                        return False
        return False

    def _updateFirstPosting(self, key, rawAmount):
        postings = self._cache[key]['postings'].copy()
        act = postings[0].account
        amt = Amount(Decimal(rawAmount), "USD")
        pst = bcdata.Posting(act, amt, None, None, None, None)
        postings[0] = pst
        self._cache[key]['postings'] = postings

    def _clearLastPosting(self, key):
        postings = self._cache[key]['postings'].copy()
        act = postings[-1].account
        pst = bcdata.Posting(act, None, None, None, None, None)
        postings[-1] = pst
        self._cache[key]['postings'] = postings

    def _promptCachedPostings(self, key, rawAmount):
        print("  Cached postings are: ")
        for posting in self._cache[key]['postings']:
            print("      {} {}".format(posting.account, posting.units))
        ans = input("  Keep (y or blank) or specify new  postings (n)? ")
        if ans == '' or ans.lower()[0] == 'y':
            self._updateFirstPosting(key, rawAmount)
            return
        self._promptPostings(key, rawAmount)


    def _writeEntry(self, entry):
        d = entry.date
        format_data = {'year':d.year, 
                       'month': d.month, 
                       'day': d.day,
                       'narration': entry.narration}

        out = '{year}-{month:02}-{day:02} * "{narration}"'.format(**format_data)

        posting_format = "\n    {{account:<{}}}{{fill: <{}}}{{amount:>{}}}"
        for posting in entry.postings:
            act = posting.account
            amt = str(posting.units) if not posting.units is None else ''
            if len(amt) > 0:
                fill_len = 81 - 4 - len(act) - len(amt)
            else:
                fill_len = 0
            posting_line = \
                posting_format.format( len(act), fill_len, len(amt))
            format_data = {'account':act,
                           'amount': amt,
                           'fill': ''}
            out += posting_line.format(**format_data)
        with open('processed_transactions.beancount', 'a') as outfile:
            outfile.write(out+'\n\n')

if __name__ == '__main__':
    if (not (len(sys.argv) == 2 or len(sys.argv) == 3)):
        print("You must provide at least one argument, pointing to the datafile.\n"
              "An optional second argument can specify the beancount journal file\n"
              " for searching previous transactions")
        sys.exit(1)

    fname = sys.argv[1]
    if (not os.path.isfile(fname)):
        print("You must provide a valid file as the argument to this script.")
        sys.exit(1)

    if len(sys.argv) == 3:
        journalFileName = sys.argv[2]
        if (not os.path.isfile(journalFileName)):
            print("That journal file does not appear to exist")
            sys.exit(1)
    else:
        journalFileName = None

    myImporter = SECU_Importer(fname, journalFileName)
    myImporter.extract()
