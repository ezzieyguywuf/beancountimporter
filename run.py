import urwid

def exit_on_q(key):
    if type(key) == type(tuple()):
        return
    if key.lower() == 'q':
        raise urwid.ExitMainLoop()

class QuestionBox(urwid.Filler):
    def keypress(self, size, key):
        if key != 'enter':
            return super(QuestionBox, self).keypress(size, key)
        self.original_widget = urwid.Text(u'Bully for you. I can\'t do {}'.format(edit.text))

class MainScreen(urwid.Filler):
    def __init__(self):
        self.top = urwid.Button(u"Top\nWith multiple\nlines")
        self.bot = urwid.Edit(u"Bot")
        self.pile = urwid.Pile([self.top, self.bot])
        super(MainScreen, self).__init__(self.pile, valign='top', min_height=80)

if __name__ == "__main__":
    main = MainScreen()
    loop = urwid.MainLoop(main, unhandled_input=exit_on_q)
    loop.run()
