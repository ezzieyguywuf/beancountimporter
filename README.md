This project will utilize the python urwid library to create a command-line importer for
beancount.

The intent of this importer will be:

* Process csv file - user must designate which columns contain pertinent info
* Allow user to supply a conversion from raw "description" to a human-readable
  "user-descr"
* Search previous transactions in beancount journal for matching "user-descr" - if found,
  use this transactions as a prototype for the new transactions that is being imported
* If no matches found, allow the user to specify the postings for the transaction.
* (maybe) keep a local cache of "user-descr" hits to speed up searching

Current Status: learning how urwid works. a very crude prototype is available in the way
of SECU_importer.py 
